var express  = require('express'),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser'),
    http = require('http'),
    sha1 = require('sha1'),
    fs = require('fs'),
    cloudinary = require('cloudinary');

    AdminSchema = new mongoose.Schema({
      email     : { type: String, required: true, unique: true },
      pwd       : { type: String, required: true },
      name      : { type: String, required: true }
    }),

    Admin = mongoose.model('Admin', AdminSchema);

    UserSchema = new mongoose.Schema({
      fullname       : String,
      email          : { type: String, required: true, unique: true },
      pwd            : { type: String, required: true },
      number         : { type: String, required: true, unique: true},
      gender         : String,
      avator         : String,
      userstatus     : Boolean
    }),
    User = mongoose.model('User', UserSchema);

    Suspected = new mongoose.Schema({
      user_id               : {type: mongoose.Schema.Types.ObjectId, ref: 'User' },
      name                  : { type: String, required: true},
      description           : { type: String},
      lat                   : { type: String},
      lng                   : { type: String},
      copy_friend           : { type: String},
      image                 : { type: String},
      address               : { type: String},
      lat                   : { type: String},
      lng                   : { type: String},
      like_count            : { type: String},
      dislike_count         : { type: String},
      confirm_status        : { type: String},
      date_time             : { type: String}
    }),
    Suspected = mongoose.model('Suspected', Suspected);

    Like = new mongoose.Schema({
      sus_id                : {type: mongoose.Schema.Types.ObjectId, ref: 'Suspected' },
      user_id               : {type: mongoose.Schema.Types.ObjectId, ref: 'User' },
      date_time             : {type: String}
    }),
    Like = mongoose.model('Like', Like);

    Dislike = new mongoose.Schema({
      sus_id                : {type: mongoose.Schema.Types.ObjectId, ref: 'Suspected' },
      user_id               : {type: mongoose.Schema.Types.ObjectId, ref: 'User' },
      date_time             : {type: String}
    }),
    Dislike = mongoose.model('Dislike', Dislike);

    ImageSchema = new mongoose.Schema({
      path       : { type: String, required: true }
    }),

    Images = mongoose.model('Images', ImageSchema);


    Setting = new mongoose.Schema({
      user_id                    : {type: mongoose.Schema.Types.ObjectId, ref: 'User' },
      notify_all_created_actions : { type: Boolean},
      notify_only_my_actions     : { type: Boolean},
      notify_confirmed_actions   : { type: Boolean},
      distance                   : { type: String},
      enable_anonymous_posting   : { type: Boolean},
      enable_invisible_mode      : { type: Boolean}
    }),
    Setting = mongoose.model('Setting', Setting);


    Chat = new mongoose.Schema({
      user_id1                    : {type: mongoose.Schema.Types.ObjectId, ref: 'User' },
      user_id2                    : {type: mongoose.Schema.Types.ObjectId, ref: 'User' },
      sended_by                   : {type: mongoose.Schema.Types.ObjectId, ref: 'User' },
      last_msg                    : {type: String},
      date_time                   : {type: String},
      isRead                      : {type: Boolean}
    }),
    Chat = mongoose.model('Chat', Chat);

    Chatdata = new mongoose.Schema({
      chat_id                      : {type: mongoose.Schema.Types.ObjectId, ref: 'Chat' },
      sended_by                    : {type: mongoose.Schema.Types.ObjectId, ref: 'User' },
      msg                          : {type: String},
      date_time                    : {type: String},
      isRead                       : {type: Boolean}
    }),
    Chatdata = mongoose.model('Chatdata', Chatdata);

   Suspectchatdata = new mongoose.Schema({
    sus_id                       : {type: mongoose.Schema.Types.ObjectId, ref: 'Suspected' },
    sended_by                    : {type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    msg                          : {type: String},
    date_time                    : {type: String},
    isRead                       : {type: Boolean}
  }),
  Suspectchatdata = mongoose.model('Suspectchatdata', Suspectchatdata);

  Friend = new mongoose.Schema({
    user_id                      : {type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    friend_id                    : {type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    date_time                    : {type: String},
  }),
  Friend = mongoose.model('Friend', Friend);

  Confirm_suspect = new mongoose.Schema({
    sus_id                      : {type: mongoose.Schema.Types.ObjectId, ref: 'Suspected' },
    user_id                     : {type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    date_time                   : {type: String},
  }),
  Confirm_suspect = mongoose.model('Confirm_suspect', Confirm_suspect);

var multer     =       require('multer');

var uristring =
process.env.MONGOLAB_URI ||
process.env.MONGOHQ_URL ||
'mongodb://0.0.0.0:27017/vigilantes';

/*var uristring =
process.env.MONGOLAB_URI ||
process.env.MONGOHQ_URL ||
'mongodb://heroku_00vnmhs0:emaster@123#@ds011268.mongolab.com:11268/heroku_00vnmhs0';*/

var upload      =     multer({ dest: 'uploadimage/'});

var db;

mongoose.connect(uristring, function (err, res) {
  if (err) {
    console.log ('ERROR connecting to: ' + uristring + '. ' + err);
  } else {
    console.log ('Succeeded connected to: ' + uristring);
    db = res;
  }
});
/*hello*/
cloudinary.config({ 
  cloud_name: 'hxh7865ld', 
  api_key: '177311831149685', 
  api_secret: 'l8ylD8jcNSdqD1SAcYPrvl4wPVk' 
});
 
express()
  .use(express.bodyParser({limit: '50mb'}))
  // Add 
  .use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
})
  .use(bodyParser.json()) // support json encoded bodies
  .use(bodyParser.urlencoded({ extended: true })) // support encoded bodies
  .use(multer({ dest: 'uploadimage/',
    rename: function (fieldname, filename) {
      return filename+Date.now();
    },
    onFileUploadStart: function (file) {
      //console.log(file.originalname + ' is starting ...');
    },
    onFileUploadComplete: function (file) {
      //console.log(file.fieldname + ' uploaded to  ' + file.path)
    }
  }))
  .post('/api/adminlogin',function(req, res){
    var email       = req.body.email;
    var pwd         = req.body.pwd;

    Admin.find({'email':email, 'pwd':pwd},{},function(e,docs){
      if(docs.length!=0){
          res.send({ result: true, data: docs });
        }else{
          res.send({ result: false, data: "Invalid Credentials"});  
        }
    });
  })

  .post('/api/userlogin',function(req, res){
    var email       = req.body.email;
    var pwd         = req.body.pwd;

    User.find({'email':email, 'pwd':pwd},{},function(e,docs){
      if(docs.length!=0){
          res.send({ result: true, data: docs });
        }else{
          res.send({ result: false, data: "Invalid Credentials"});  
        }
    });
  })

  .post('/api/addadmin',function(req, res){

    var fullname    = req.body.fullname;
    var email       = req.body.email;
    var pwd         = req.body.pwd;

    var admin = new Admin({'name':fullname, 'email':email, 'pwd':pwd});
    admin.id = admin._id;
    admin.save(function (err) {
        res.send(
            (err === null) ? { result: true, data: admin  } : {result: false, msg: err }
        );
    });
  })

  .post('/api/contact',function(req, res){
    var user_list=[];
    var contacts    = req.body.contact;
    var user_id    = req.body.user_id;
    var contact=JSON.parse(contacts);
    //console.log(contact.length);
     function test(s){
      if(s<contact.length){
          if(contact[s].phoneNumbers!=null){
              var f_contact=contact[s].phoneNumbers[0].value;
              //console.log(f_contact);
              User.find({'number':f_contact},{},function(e,docs){
                if(docs.length!=0){
                    //console.log(docs);
                    var fri_id=docs[0]._id;
                    Friend.find({'user_id':user_id},{'friend_id':fri_id},{},function(e,docs){
                    if(docs.length==0){
                          var friend = new Friend({'user_id':user_id, 'friend_id':fri_id});
                            friend.id = friend._id;
                            friend.save(function (err) {
                                     user_list.push({"name":docs[0].fullname,"ot_user_id":fri_id});
                                });
                      }
                  });
                  //console.log(user_list);
                    s++;
                    test(s);
                }else{
                    s++;
                    test(s);
                }
             });
          }else{
                s++;
                test(s);
          }
          
      }else{
        res.send({ result: true, data: user_list });
      }
      
    }

    test(0);
  })
        
  .post('/api/adduser',function(req, res){

    var fullname           = req.body.fullname;
    var email              = req.body.email;
    var pwd                = req.body.pwd;
    var number             = req.body.number;

    User.find({'email':email},{},function(e,docs){
      if(docs.length!=0){
            res.send({ result: false, data:"Email already exist." });
        }else
        {
            User.find({'number':number},{},function(e,docs){
                    if(docs.length!=0){
                          res.send({ result: false, data:"Number already exist." });
                      }else
                      {
                          var user = new User({'fullname':fullname, 'email':email, 'pwd':pwd,'number':number});
                          user.id = user._id;
                          user.save(function (err) {
                              var setting = new Setting({'user_id':user._id,"notify_all_created_actions":true, "notify_only_my_actions":true,"notify_confirmed_actions":true,"distance":'50',"enable_anonymous_posting":true,"enable_invisible_mode":true});
                              setting.id = setting._id;
                              setting.save(function (err) {
                                  /*res.send(
                                      (err === null) ? { result: true, data: setting  } : {result: false, msg: err }
                                  );*/
                              });
                              res.send(
                                  (err === null) ? { result: true, data: user  } : {result: false, msg: err }
                              );
                          });
                      }
                  });
          }
    });
  })

  .post('/api/updateprofile',function(req, res){

    var user_id           = req.body.user_id;
    var fullname           = req.body.name;
    var email              = req.body.email;
    var number             = req.body.number;
    var image              = req.body.image;

    User.find({'email':email},{},function(e,docs){
      if(docs.length!=0){
        console.log(docs[0]._id);
            if(docs[0]._id!=user_id)
            {
              res.send({ result: false, data:"Email already exist." });
            }else
            {
               User.find({'number':number},{},function(e,docs){
                    if(docs.length!=0){
                           if(docs[0]._id!=user_id)
                            {
                              res.send({ result: false, data:"Number already exist." });
                            }else
                            {
                              User.update({'_id':user_id}, {$set: {"fullname":fullname, "email":email, "number":number, "avator":image}},function(e, docs){
                                  res.send(
                                    (e === null) ? { result: true, data: docs  } : {result: false, msg: e }
                                  );
                              });
                            }
                          
                      }
                  });

            }
        }else
        {
          User.update({'_id':user_id}, {$set: {"fullname":fullname, "email":email, "number":number}},function(e, docs){
                                  res.send(
                                    (e === null) ? { result: true, data: docs  } : {result: false, msg: e }
                                  );
                              });

        }
    });
   })

  .post('/api/addmember',function(req, res){

    var fullname           = req.body.name;
    var email              = req.body.email;
    var number            = req.body.contact;

    User.find({'email':email},{},function(e,docs){
      if(docs.length!=0){
            res.send({ result: false, data:"Email already exist." });
        }else
        {
            User.find({'number':number},{},function(e,docs){
                    if(docs.length!=0){
                          res.send({ result: false, data:"Number already exist." });
                      }else
                      {
                          var user = new User({'fullname':fullname, 'email':email,'number':number,'pwd':'123456'});
                          user.id = user._id;
                          user.save(function (err) {
                              var setting = new Setting({'user_id':user._id,"notify_all_created_actions":true, "notify_only_my_actions":true,"notify_confirmed_actions":true,"distance":'50',"enable_anonymous_posting":true,"enable_invisible_mode":true});
                              setting.id = setting._id;
                              setting.save(function (err) {
                                  /*res.send(
                                      (err === null) ? { result: true, data: setting  } : {result: false, msg: err }
                                  );*/
                              });
                              res.send(
                                  (err === null) ? { result: true, data: user  } : {result: false, msg: err }
                              );
                          });
                      }
                  });
          }
    });
   })

  .post('/api/addlike',function(req, res){

    var user_id                   = req.body.user_id;
    var sus_id                    = req.body.sus_id;


    var date = new Date();

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    var date_time = year + ":" + month + ":" + day + " " + hour + ":" + min + ":" + sec;

    //var newimage  = image;//"http://koodoit1.herokuapp.com/"+user_pic;

    var count=0;
    var dislike_count=0;
    Suspected.find({'_id':sus_id},{},function(e,docs){
          if(docs.like_count==null ){
             count=0;
           }else{
                count=docs.like_count;
                count++;
                dislike_count=docs.dislike_count;
                if(dislike_count==0){
                   dislike_count=0;
                }else{
                  dislike_count=docs.dislike_count;
                  dislike_count--;
                }
            }

            Suspected.update({'_id':sus_id}, {$set: {"like_count":count, "dislike_count":dislike_count}},function(e, docs){
              
          });
        
    });

    Dislike.remove({'sus_id':sus_id,'user_id':user_id},function(e,docs){
          
      });

    var like = new Like({'sus_id':sus_id,'user_id':user_id,'date_time':date_time});
    like.id = like._id;
    like.save(function (err) {
        res.send(
            (err === null) ? { result: true, data: like  } : {result: false, msg: err }
        );
    });
  })

  .post('/api/dislike',function(req, res){

    var user_id                 = req.body.user_id;
    var sus_id                  = req.body.sus_id;


    var date = new Date();

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    var date_time = year + ":" + month + ":" + day + " " + hour + ":" + min + ":" + sec;

    //var newimage  = image;//"http://koodoit1.herokuapp.com/"+user_pic;


    var count=0;
    var dislike_count=0;
    Suspected.find({'_id':sus_id},{},function(e,docs){
          if(docs.like_count==null ){
             count=0;
           }else{
                count=docs.dislike_count;
                dislike_count++;
                count=docs.count;
                if(count==0){
                   count=0;
                }else{
                  count=docs.count;
                  count--;
                }
            }

            Suspected.update({'_id':sus_id}, {$set: {"like_count":count, "dislike_count":dislike_count}},function(e, docs){
              
          });
        
    });

     Like.remove({'sus_id':sus_id,'user_id':user_id},function(e,docs){
          
      });

    var dislike = new Dislike({'sus_id':sus_id, 'user_id':user_id,'date_time':date_time});
    dislike.id = dislike._id;
    dislike.save(function (err) {
        res.send(
            (err === null) ? { result: true, data: dislike  } : {result: false, msg: err }
        );
    });
  })

  .post('/api/addsuspected',function(req, res){

    var user_id                 = req.body.user_id;
    var name                    = req.body.name;
    var address                 = req.body.address;
    var lat                     = req.body.lat;
    var lng                     = req.body.lng;
    var copy_friend             = req.body.copy_friend;
    var description             = req.body.description;
    var image                   = req.body.image;


    var date = new Date();

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    var date_time = year + ":" + month + ":" + day + " " + hour + ":" + min + ":" + sec;

    //var newimage  = image;//"http://koodoit1.herokuapp.com/"+user_pic;
    

    var suspected = new Suspected({'user_id':user_id, 'name':name, 'address':address,'lat':lat,'lng':lng,'copy_friend':copy_friend,'description':description,'image':image,'date_time':date_time,'like_count':'0','dislike_count':'0','confirm_status':'0'});
    suspected.id = suspected._id;
    suspected.save(function (err) {
        res.send(
            (err === null) ? { result: true, data: suspected  } : {result: false, msg: err }
        );
    });
  })

  .post('/api/confirm_suspect',function(req, res){

      var user_id                     = req.body.user_id;
      var sus_id                      = req.body.sus_id;


      var date = new Date();

      var hour = date.getHours();
      hour = (hour < 10 ? "0" : "") + hour;

      var min  = date.getMinutes();
      min = (min < 10 ? "0" : "") + min;

      var sec  = date.getSeconds();
      sec = (sec < 10 ? "0" : "") + sec;

      var year = date.getFullYear();

      var month = date.getMonth() + 1;
      month = (month < 10 ? "0" : "") + month;

      var day  = date.getDate();
      day = (day < 10 ? "0" : "") + day;

      var date_time = year + ":" + month + ":" + day + " " + hour + ":" + min + ":" + sec;

      Confirm_suspect.find({$and: [{'user_id':user_id},{'sus_id':sus_id}] },{},function(e,docs){
        if(docs.length!=0){
              res.send({ result: false, data:"Suspect already Confirm." });
          }else
          {
            var confirm_suspect = new Confirm_suspect({'user_id':user_id, 'sus_id':sus_id, 'date_time':date_time});
                confirm_suspect.id = confirm_suspect._id;
                confirm_suspect.save(function (err) {
                         Suspected.update({'_id':sus_id}, {$set: {"confirm_status":'1'}},function(e, docs){
                        res.send(
                          (e === null) ? { result: true, data: docs  } : {result: false, msg: e }
                        );
                    });
                 });
             }
      });
   })

  .post('/api/check_confirm_suspect',function(req, res){
    var user_id       = req.body.user_id;
    Suspected.find({'user_id':user_id,'confirm_status':'0','dislike_count':'0',like_count: { $gt: '4' } },{},function(e,docs){
      if(docs!=undefined){
            res.send({ result: true, data: docs });
          }else{
            res.send({ result: false, data: "Invalid Credentials"});  
          }
    });
  })

  .post('/api/getlike',function(req, res){
    var user_id       = req.body.user_id;
    var sus_id        = req.body.sus_id;
    Like.find({'user_id':user_id,'sus_id':sus_id},{},function(e,docs){
      if(docs.length!=0){
          res.send({ result: true, data: docs });
         
       }else{
          res.send({ result: false, data: "Invalid Credentials"});  
        }
    });
  })

  .post('/api/allgetlike',function(req, res){
    var user_id       = req.body.user_id;
    var sus_id        = req.body.sus_id;
    Like.find({},{},function(e,docs){
      if(docs.length!=0){
          res.send({ result: true, data: docs });
         
       }else{
          res.send({ result: false, data: "Invalid Credentials"});  
        }
    });
  })

  .post('/api/getdislike',function(req, res){
    var user_id       = req.body.user_id;
    var sus_id        = req.body.sus_id;
    Dislike.find({'user_id':user_id,'sus_id':sus_id},{},function(e,docs){
      if(docs.length!=0){
          res.send({ result: true, data: docs });
         
       }else{
          res.send({ result: false, data: "Invalid Credentials"});  
        }
    });
  })

  .post('/api/setting',function(req, res){

      var user_id                     = req.body.user_id;
      var notify_all_created_actions  = req.body.notify_all_created_actions;
      var notify_only_my_actions      = req.body.notify_only_my_actions;
      var notify_confirmed_actions    = req.body.notify_confirmed_actions;
      var distance                    = req.body.distance;
      var enable_anonymous_posting    = req.body.enable_anonymous_posting;
      var enable_invisible_mode       = req.body.enable_invisible_mode;

      Setting.find({'user_id':user_id},{},function(e,docs){
        if(docs.length!=0){

              Setting.update({'user_id':user_id}, {$set: {"notify_all_created_actions":notify_all_created_actions, "notify_only_my_actions":notify_only_my_actions,"notify_confirmed_actions":notify_confirmed_actions,"distance":distance,"enable_anonymous_posting":enable_anonymous_posting,"enable_invisible_mode":enable_invisible_mode}},function(e, docs){
                res.send(
                  (e === null) ? { result: true, data: docs  } : {result: false, msg: e }
                );
            });
              
          }else
          {
            var setting = new Setting({'user_id':user_id,"notify_all_created_actions":notify_all_created_actions, "notify_only_my_actions":notify_only_my_actions,"notify_confirmed_actions":notify_confirmed_actions,"distance":distance,"enable_anonymous_posting":enable_anonymous_posting,"enable_invisible_mode":enable_invisible_mode});
              setting.id = setting._id;
              setting.save(function (err) {
                  res.send(
                      (err === null) ? { result: true, data: setting  } : {result: false, msg: err }
                  );
              });
          }
      });
  })

  .post('/api/getsetting',function(req, res){
    var user_id       = req.body.user_id;
    Setting.find({'user_id':user_id},{},function(e,docs){
      if(docs.length!=0){
          res.send({ result: true, data: docs });
         
       }else{
          res.send({ result: false, data: "Invalid Credentials"});  
        }
    });
  })

.post('/api/allsetting',function(req, res){
    Setting.find({},{},function(e,docs){
      if(docs.length!=0){
          res.send({ result: true, data: docs });
         
       }else{
          res.send({ result: false, data: "Invalid Credentials"});  
        }
    });
  })

  .post('/api/addimage',function(req, res){

    var image    = req.body.image;

    cloudinary.uploader.upload(image, function(result) { 
    var path = result.secure_url;

    var images = new Images({'path':path});
    images.id = images._id;
    images.save(function (err) {
        res.send(
            (err === null) ? { result: true, data: images  } : {result: false, msg: err }
        );
    });
    });
  })

  .post('/api/imageprofile',function(req, res){

      var image    = req.body.image;

      cloudinary.uploader.upload(image, function(result) { 
      var path = result.secure_url;

      res.send( { result: true, data: path  });

      });
  })

  .post('/api/allimage',function(req, res){
      Images.find({},{},function(e,docs){
          res.send(
            (e === null) ? { result: true, data: docs  } : {result: false, msg: e }
          );
      });
  })

  .post('/api/alluser',function(req, res){
      User.find({},{},function(e,docs){
          res.send(
            (e === null) ? { result: true, data: docs  } : {result: false, msg: e }
          );
      });
  })

  .post('/api/allsuspected',function(req, res){
      Suspected.find({'confirm_status':'0'},{},function(e,docs){
          res.send(
            (e === null) ? { result: true, data: docs  } : {result: false, msg: e }
          );
      });
  })

  .post('/api/pull_to_refresh_suspected',function(req, res){
      var last_id       = req.body.last_id;
      Suspected.find({ _id: { $gt: last_id } },{},function(e,docs){
          res.send(
            (e === null) ? { result: true, data: docs  } : {result: false, msg: e }
          );
      });
  })

  .post('/api/allconfirmsuspected',function(req, res){

      Confirm_suspect.find({})
            .populate('sus_id')
            .exec(function(e, docs) {
                 if(docs!=undefined){
              res.send({ result: true, data: docs });
            }else{
              res.send({ result: false, data: "Invalid Credentials"});  
            }
          });
  })

  .post('/api/pull_to_refresh_confirmsuspected',function(req, res){
      var last_id       = req.body.last_id;
      Suspected.find({ _id: { $gt: last_id } },{},function(e,docs){
          res.send(
            (e === null) ? { result: true, data: docs  } : {result: false, msg: e }
          );
      });
  })

  .post('/api/getsuspected',function(req, res){
    var sus_id       = req.body.sus_id;
    Suspected.find({'_id':sus_id},{},function(e,docs){
      if(e==null){
          res.send({ result: true, data: docs });
         
       }else{
          res.send({ result: false, data: "Invalid Credentials"});  
        }
    });
  })

  .post('/api/getsuspectedByUserId',function(req, res){
    var user_id       = req.body.user_id;
    Suspected.find({'user_id':user_id},{},function(e,docs){
      if(e==null){
          res.send({ result: true, data: docs });
         
       }else{
          res.send({ result: false, data: "Invalid Credentials"});  
        }
    });
  })

  .post('/api/editsuspected',function(req, res){
    var user_id       = req.body.user_id;
    var title       = req.body.title;
    var description       = req.body.description;

    Suspected.update({'_id':user_id}, {$set: {"name":title, "description":description}},function(e, docs){
            res.send(
              (e === null) ? { result: true, data: docs  } : {result: false, msg: e }
            );
        });
  })

  .post('/api/getuser',function(req, res){
    var user_id       = req.body.user_id;

    User.find({'_id':user_id},{},function(e,docs){
      console.log(docs);
      if(docs!=undefined){
          res.send({ result: true, data: docs });
        }else{
          res.send({ result: false, data: "Invalid Credentials"});  
        }
    });
  })

  .post('/api/getaddfriend',function(req, res){
    var user_value       = req.body.user_value;

    User.find({ $or: [ { email:user_value }, { number: user_value } ] },{},function(e,docs){
      console.log(docs);
      if(docs!=undefined){
          res.send({ result: true, data: docs });
        }else{
          res.send({ result: false, data: "Invalid Credentials"});  
        }
    });
  })

  .post('/api/getimage',function(req, res){
    var image_id       = req.body.image_id;

    Images.find({'_id':image_id},{},function(e,docs){
      if(e==null){
          res.send({ result: true, data: docs });
        }else{
          res.send({ result: false, data: "Invalid Credentials"});  
        }
    });
  })

  .post('/api/edituser',function(req, res){
    var user_id       = req.body.user_id;
    var fullname       = req.body.fullname;
    var username       = req.body.username;
    var number       = req.body.number;

    User.update({'_id':user_id}, {$set: {"fullname":fullname, "username":username, "number":number}},function(e, docs){
            res.send(
              (e === null) ? { result: true, data: docs  } : {result: false, msg: e }
            );
        });
  })

  .post('/api/deleteUser',function(req, res){
      var user_id       = req.body.user_id;
      User.remove({'_id':user_id},function(e,docs){
          res.send(
            (e === null) ? { result: true, data: docs  } : {result: false, msg: e }
          );
      });
  })

  .post('/api/alldeleteUser',function(req, res){
      var user_id       = req.body.user_id;
      User.remove({},function(e,docs){
          res.send(
            (e === null) ? { result: true, data: docs  } : {result: false, msg: e }
          );
      });
  })

  .post('/api/deleteImage',function(req, res){
      Images.remove({},function(e,docs){
          res.send(
            (e === null) ? { result: true, data: docs  } : {result: false, msg: e }
          );
      });
  })

  .post('/api/deleteSuspected',function(req, res){
      var user_id       = req.body.user_id;
      Suspected.remove({'_id':user_id},function(e,docs){
          res.send(
            (e === null) ? { result: true, data: docs  } : {result: false, msg: e }
          );
      });
  })

  .post('/api/AlldeleteSuspected',function(req, res){
      var user_id       = req.body.user_id;
      Suspected.remove({},function(e,docs){
          res.send(
            (e === null) ? { result: true, data: docs  } : {result: false, msg: e }
          );
      });
  })
  
  .post('/api/addchat',function(req, res){

    var chat_id                    = req.body.chat_id;
    var user_id1                   = req.body.user_id1;
    var user_id2                   = req.body.user_id2;
    var msg                        = req.body.msg;


    var date = new Date();

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    var date_time = year + ":" + month + ":" + day + " " + hour + ":" + min + ":" + sec;

    //var newimage  = image;//"http://koodoit1.herokuapp.com/"+user_pic;

    var count=0;
    var dislike_count=0;

    Chat.find({$or:[
              {$and:[{'user_id1':user_id1},{'user_id2':user_id2}]},
              {$and:[{'user_id2':user_id1},{'user_id1':user_id2}]}
             ]},function(e,docs){
              if(docs.length!=0)
              {
                var chat_id=docs[0]._id;
              }
        //console.log(docs[0]._id);
        if(docs!=undefined && docs.length==0){

            var chat = new Chat({'user_id1':user_id1, 'user_id2':user_id2, 'last_msg':msg, 'date_time':date_time, 'sended_by':user_id2});
                chat.id = chat._id;
                chat.save(function (err) {

                       var chatdata = new Chatdata({'chat_id':chat._id, 'sended_by':user_id1, 'msg':msg, 'date_time':date_time});
                          chatdata.id = chatdata._id;
                          chatdata.save(function (err) {
                                  res.send(
                                      (err === null) ? { result: true, data: chatdata  } : {result: false, msg: err }
                                  )
                              });

                });
            
           }else{
                
                 Chat.update({'_id':chat_id}, {$set: {'user_id1':user_id1, 'user_id2':user_id2, 'last_msg':msg, 'date_time':date_time, 'sended_by':user_id2}},function(e, docs){

                      var chatdata = new Chatdata({'chat_id':chat_id, 'sended_by':user_id1, 'msg':msg, 'date_time':date_time});
                          chatdata.id = chatdata._id;
                          chatdata.save(function (err) {
                                  res.send(
                                      (err === null) ? { result: true, data: chatdata  } : {result: false, msg: err }
                                  )
                              });
              
                 });
                
            }

      });
  })

  .post('/api/getchathistory',function(req, res){
      var user_id1       = req.body.user_id1;
      var user_id2       = req.body.user_id2;

        Chat.find({$or:[
                {$and:[{'user_id1':user_id1},{'user_id2':user_id2}]},
                {$and:[{'user_id2':user_id1},{'user_id1':user_id2}]}
               ]})
              .populate('user_id1')
              .populate('user_id2')
              .populate('sended_by')
              .exec(function(e, docs) {
                   if(docs!=undefined){
                res.send({ result: true, data: docs });
              }else{
                res.send({ result: false, data: "Invalid Credentials"});  
              }
            });
  })

  .post('/api/getchatdata',function(req, res){
    var chat_id       = req.body.chat_id;

      Chatdata.find({'chat_id':chat_id},function(e,docs){
       // console.log(docs);
        if(docs!=undefined){
            res.send({ result: true, data: docs });
          }else{
            res.send({ result: false, data: "Invalid Credentials"});  
          }
      });
  })

  .post('/api/lastgetchatdata',function(req, res){
    var last_id       = req.body.last_id;

      Chatdata.find({ _id: { $gt: last_id } })
            .populate('sended_by')
            .exec(function(e, docs) {
                 if(docs!=undefined){
              res.send({ result: true, data: docs });
            }else{
              res.send({ result: false, data: "Invalid Credentials"});  
            }
          });
  })

  .post('/api/addsuspectchat',function(req, res){

    var sus_id                     = req.body.sus_id;
    var sended_by                  = req.body.sended_by;
    var msg                        = req.body.msg;


    var date = new Date();

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    var date_time = year + ":" + month + ":" + day + " " + hour + ":" + min + ":" + sec;

    var suspectchatdata = new Suspectchatdata({'sus_id':sus_id, 'sended_by':sended_by, 'msg':msg, 'date_time':date_time});
        suspectchatdata.id = suspectchatdata._id;
        suspectchatdata.save(function (err) {
                res.send(
                    (err === null) ? { result: true, data: suspectchatdata  } : {result: false, msg: err }
                )
            });
  })

  .post('/api/getsuspectchatdata',function(req, res){
      var sus_id       = req.body.sus_id;

        Suspectchatdata.find({'sus_id':sus_id})
              .populate('sended_by')
              .exec(function(e, docs) {
                   if(docs!=undefined){
                res.send({ result: true, data: docs });
              }else{
                res.send({ result: false, data: "Invalid Credentials"});  
              }
            });
  })

  .post('/api/lastgetsuspectchatdata',function(req, res){
      var last_id       = req.body.last_id;

        Suspectchatdata.find({ _id: { $gt: last_id } })
              .populate('sended_by')
              .exec(function(e, docs) {
                   if(docs!=undefined){
                res.send({ result: true, data: docs });
              }else{
                res.send({ result: false, data: "Invalid Credentials"});  
              }
            });
  })

  .post('/api/allgetchatdata',function(req, res){
    var chat_id       = req.body.chat_id;

      Chatdata.find({},function(e,docs){
       // console.log(docs);
        if(docs!=undefined){
            res.send({ result: true, data: docs });
          }else{
            res.send({ result: false, data: "Invalid Credentials"});  
          }
      });
  })

  .post('/api/addfriend',function(req, res){

      var user_id                     = req.body.user_id;
      var friend_id                  = req.body.friend_id;


      var date = new Date();

      var hour = date.getHours();
      hour = (hour < 10 ? "0" : "") + hour;

      var min  = date.getMinutes();
      min = (min < 10 ? "0" : "") + min;

      var sec  = date.getSeconds();
      sec = (sec < 10 ? "0" : "") + sec;

      var year = date.getFullYear();

      var month = date.getMonth() + 1;
      month = (month < 10 ? "0" : "") + month;

      var day  = date.getDate();
      day = (day < 10 ? "0" : "") + day;

      var date_time = year + ":" + month + ":" + day + " " + hour + ":" + min + ":" + sec;

      Friend.find({$and: [{'user_id':user_id},{'friend_id':friend_id}] },{},function(e,docs){
        if(docs.length!=0){
              res.send({ result: false, data:"User already exist." });
          }else
          {
            var friend = new Friend({'user_id':user_id, 'friend_id':friend_id, 'date_time':date_time});
                friend.id = friend._id;
                friend.save(function (err) {
                        res.send(
                            (err === null) ? { result: true, data: friend  } : {result: false, msg: err }
                        )
                    });
          }
      });
  })

  .post('/api/getfriend',function(req, res){
    var user_id       = req.body.user_id;

      Friend.find({'user_id':user_id})
            .populate('friend_id')
            .exec(function(e, docs) {
                 if(docs!=undefined){
              res.send({ result: true, data: docs });
            }else{
              res.send({ result: false, data: "Invalid Credentials"});  
            }
          });
   })

  .post('/api/forgotpassword',function(req, res){

      var email       = req.body.email;

      User.find({'email':email},{},function(e,docs){
      if(docs.length!=0){

            var pwd = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            for( var i=0; i < 5; i++ )
            {
                pwd += possible.charAt(Math.floor(Math.random() * possible.length));
            }

             var options = {
              host: 'engineermaster.in',
              port: null,
              path: '/vigilantes/mail.php?email='+email+'&pwd='+pwd
            };
            http.get(options, function(resp){
              resp.on('data', function(chunk){
                    User.update({'email':email}, {$set: {"pwd":pwd}},function(e, docs){
                        res.send(
                          (e === null) ? { result: true, data: docs  } : {result: false, msg: e }
                        );
                    });
              });
            }).on("error", function(e){
              console.log("Got error: " + e.message);
            });
        }else{
          res.send({ result: false, data: "Invalid Email"});  
        }
    });
})

.use(express.static(__dirname + '/'))
.listen(process.env.PORT || 5000);
