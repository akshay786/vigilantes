var app = angular.module('vigilates', ['ui.router','LoginCtrl.controllers','HomeCtrl.controllers','useCtrl.controllers','nav.controllers','suspected.controllers','editsuspected.controllers','edituser.controllers'])

app.run(function($rootScope){
	$rootScope.dataPrintRunRoot = "CHECKING";
});

app.config(function ($stateProvider, $urlRouterProvider) {
	'use strict';

	$stateProvider
		    .state('login', {
		        url: "/login",
		        templateUrl: "templates/login.html",
		        controller: "LoginCtrl"
		    })
		    .state('nav', {
		        url: "/nav",
		        templateUrl: "templates/nav.html",
		        controller: "navController",
		        abstract: true
		    })
		    .state('nav.home', {
		        url: '/home',
		        templateUrl: "templates/home.html",
			    controller: 'HomeCtrl'
		    })
		    .state('nav.user', {
		        url: '/user',
		        templateUrl: "templates/users.html",
			    controller: 'useCtrl'
		    })
		    .state('nav.changepassword', {
		        url: '/changepassword',
		        templateUrl: "templates/changepassword.html",
			    controller: 'changepasswordCtrl'
		    })
		    .state('nav.suspected', {
		        url: '/suspected',
		        templateUrl: "templates/suspected.html",
			    controller: 'suspectedController'
		    })
		    .state('nav.editsuspected', {
		        url: '/editsuspected',
		        templateUrl: "templates/editsuspected.html",
			    controller: 'editsuspectedController'
		    })
		    .state('nav.edituser', {
		        url: '/edituser',
		        templateUrl: "templates/edituser.html",
			    controller: 'edituserController'
		    })
		    .state('nav.contact-us', {
		        url: '/contact-us',
		        templateUrl: "templates/contact-us.html",
			    controller: 'contactController'
		    });
		    
		$urlRouterProvider.otherwise('/login');

	// var otherwiseURL = "/";
	// var isLogin = localStorage.getItem("isLogin");
	// if(isLogin=="1"){
	// 	otherwiseURL = "/home";
	// }

	// $routeProvider
	// 	.when('/', {
	// 		controller: 'LoginCtrl',
	// 		templateUrl: 'templates/login.html'
	// 	})
	// 	.when('/home', {
	// 		controller: 'HomeCtrl',
	// 		templateUrl: 'templates/home.html'
	// 	})
	// 	.when('/user', {
	// 		controller: 'UserCtrl',
	// 		templateUrl: 'templates/user.html'
	// 	})
	// 	.when('/question', {
	// 		controller: 'QuestionCtrl',
	// 		templateUrl: 'templates/question.html'
	// 	})
	// 	.when('/changepassword', {
	// 		controller: 'ChangePasswordCtrl',
	// 		templateUrl: 'templates/changepassword.html'
	// 	})
	// 	.when('/topics', {
	// 		controller: 'TopicCtrl',
	// 		templateUrl: 'templates/topics.html'
	// 	})
	// 	.when('/questiontype', {
	// 		controller: 'QuestionTypeCtrl',
	// 		templateUrl: 'templates/question_type.html'
	// 	})
	// 	.otherwise({
	// 		redirectTo: otherwiseURL
	// 	});
});

app.factory('$localstorage', ['$window', function($window) {
    return {
	    set: function(key, value) {
	     $window.localStorage[key] = value;
	    },
	    get: function(key, defaultValue) {
	     return $window.localStorage[key] || defaultValue;
	    },
	    setObject: function(key, value) {
	     $window.localStorage[key] = JSON.stringify(value);
	    },
	    getObject: function(key) {
	     return JSON.parse($window.localStorage[key] || '{}');
	    }
    }
}]);
