angular
	.module('newchallengeCtrl.controllers', [])
	.controller('newchallengeCtrl', function ($scope, $sce, $http, $localstorage) {
  		
	    $scope.progressVisible = false;
	    $scope.afterUpload = true;
	    $scope.videoUploadSrc = "";

	    $scope.buttonDisable = true;
	    $scope.formSubmit = false;

	    $scope.name_text_show = false;
		$scope.video_alert = false;



	    var is_comp_id = $localstorage.get('is_comp_id');
	    
	    $localstorage.set('is_comp_id', 0);

	    if(is_comp_id==undefined || is_comp_id=="undefined" || is_comp_id=="null" || is_comp_id==null || is_comp_id=="" || is_comp_id=="0" || is_comp_id==0){
	    	is_comp_id = 0;
	    }

	    $scope.displayName = "Display Name";
	    $scope.NameModel = "Display Name";

	    $scope.profileImage = "images/userblank.jpg";

	    $scope.changeName = function(){
	    	$scope.name_text_show = true;
	    }

	    $scope.setName = function(){
	    	$scope.displayName = $scope.NameModel;
	    	$scope.name_text_show = false;
	    }

	    $scope.newchallengeform = {
	    	'title' 		:'',
	    	'description'	:'',
	    	'category'		:'',
	    	'exp_date'		:'3 Week',
	    	'tags'			:'',
	    	'rivals_comment':'',
	    	'videoUrl'		:'',
	    }

	    $scope.categories = [
	    	{'id':'1', 'value':'Classical'},
	    	{'id':'2', 'value':'Western'},
	    	{'id':'3', 'value':'HipHop'},
	    	{'id':'4', 'value':'Other'}
	    ];

	    $scope.progressVisibleProfile = false;
	    $scope.progressProfile = 0;

	    $scope.filesProfile = [];

	    $scope.setProfile = function(element) {
		    $scope.$apply(function(scope) {
		        $scope.filesProfile = []
		        for (var i = 0; i < element.files.length; i++) {
		          $scope.filesProfile.push(element.files[i])
		        }
		      	$scope.progressVisibleProfile = false;
		      	$scope.uploadProfile();
		   	});
		};

		$scope.uploadProfile = function() {
	        var fd = new FormData()
	        for (var i in $scope.filesProfile) {
	            fd.append("file", $scope.filesProfile[i])
	        }
	        var xhr = new XMLHttpRequest()
	        xhr.upload.addEventListener("progress", uploadProgressProfile, false)
	        xhr.addEventListener("load", uploadCompleteProfile, false)
	        xhr.addEventListener("error", uploadFailed, false)
	        xhr.addEventListener("abort", uploadCanceled, false)
	        xhr.open("POST", "/api/photo")
	        $scope.progressVisibleProfile = true;
	        xhr.send(fd);
	    }

	    function uploadProgressProfile(evt) {
	        $scope.$apply(function(){
	            if (evt.lengthComputable) {
	                $scope.progressProfile = Math.round(evt.loaded * 100 / evt.total)
	            } else {
	                $scope.progressProfile = 'unable to compute'
	            }
	        })
	    }

	    function uploadCompleteProfile(evt) {
	    	var data = evt.target.responseText;
	    	var dataObj = JSON.parse(data);
			$scope.profileImage = dataObj.file.path;
	    }

	    $scope.create_challenge_btn = "Create Challenge";

	    $scope.setFiles = function(element) {
		    $scope.$apply(function(scope) {
		        $scope.files = []
		        for (var i = 0; i < element.files.length; i++) {
		          $scope.files.push(element.files[i])
		        }
		      	$scope.progressVisible = false;
		      	$scope.uploadFile();
		   	});
		};

	    $scope.uploadFile = function() {

	    	console.log("Check");
	    	var name=$scope.files[0].name;
	    	var ext=name.split('.');
	    	var extention=ext[ext.length-1];
	    	if(extention=='MOV' || extention=='MPEG4' || extention=='MP4' || extention=='AVI' || extention=='WMV' || extention=='MPEGPS' ||extention=='FLV' || extention=='3GPP' || extention=='WebM' || extention=='mov' || extention=='mpeg4' || extention=='mp4' || extention=='avi' || extention=='wmv' || extention=='mpegps' ||extention=='flv' || extention=='3gpp' || extention=='WEBM' || extention=='webm')
	    		
	    	{
	    		var fd = new FormData()
		        for (var i in $scope.files) {
		            fd.append("file", $scope.files[i])
		        }
		        var xhr = new XMLHttpRequest()
		        xhr.upload.addEventListener("progress", uploadProgress, false)
		        xhr.addEventListener("load", uploadComplete, false)
		        xhr.addEventListener("error", uploadFailed, false)
		        xhr.addEventListener("abort", uploadCanceled, false)
		        xhr.open("POST", "/api/photo")
		        $scope.progressVisible = true;
		        xhr.send(fd);
		    }else
		    {
		    	
		    	$scope.video_alert = true;
		    	return false;
		    }
	    }

	    function uploadProgress(evt) {
	        $scope.$apply(function(){
	            if (evt.lengthComputable) {
	                $scope.progress = Math.round(evt.loaded * 100 / evt.total)
	            } else {
	                $scope.progress = 'unable to compute'
	            }
	        })
	    }

	    function uploadComplete(evt) {
	    	var data = evt.target.responseText;
	    	var dataObj = JSON.parse(data);

	    	$scope.afterUpload = false;
			$scope.buttonDisable = false;
			$scope.newchallengeform.videoUrl = dataObj.file.path;

			document.getElementById('btn_create_challenge').disabled = false;

	    	document.getElementById('divLoadButton').style.display = "none";
	    	document.getElementById('divLoadVideo').style.display = "block";
	    	var myVideo = document.getElementsByTagName('video')[0];
			myVideo.src = dataObj.file.path;
			myVideo.load();
			//myVideo.play();

	    }

	    function uploadFailed(evt) {
	        alert("There was an error attempting to upload the file.")
	    }

	    function uploadCanceled(evt) {
	        $scope.$apply(function(){
	            $scope.progressVisible = false;
	        })
	        alert("The upload has been canceled by the user or the browser dropped the connection.")
	    }

	    $scope.tagsArray = [];

	    $scope.setTag = function(text){
	    	$scope.tagsArray.push(text);
	    	$scope.newchallengeform.tags = $scope.tagsArray.toString();
	    }

	    $scope.create_challenge = function(){
	    	$scope.formSubmit = true;
	    	if($scope.newchallengeform.title.trim()!='' && $scope.newchallengeform.description.trim()!='' && $scope.newchallengeform.category.trim()!='' && $scope.newchallengeform.exp_date.trim()!=''){

	    		$scope.formSubmit = false;
	    		
	    		var dataset = {
    				"user_id": "56778851c94da5fa1a3d6509",
				    "title": $scope.newchallengeform.title,
				    "desc": $scope.newchallengeform.description,
				    "cat": $scope.newchallengeform.category,
				    "exp_date": $scope.newchallengeform.exp_date,
				    "video_url": $scope.newchallengeform.videoUrl,
				    "tags": $scope.newchallengeform.tags,
				    "rival_comment": $scope.newchallengeform.rivals_comment,
				    "comment_count": 0,
				    "competition_id":is_comp_id,
				    "user_name":$scope.NameModel,
				    "user_pic":$scope.profileImage
	    		}

	    		$scope.challenge_btn_click = true;
	    		$scope.create_challenge_btn = "Processing Please Wait..";

	    		$http({
					method: 'POST', 
					url: '/api/postvideo',
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
					transformRequest: function(obj) {
						var str = [];
						for(var p in obj)
						str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
						return str.join("&");
					},
					data: dataset
				}).success(function(data) {
					try{
						$scope.create_challenge_btn = "Create Challenge";
						$scope.challenge_btn_click = false;
						if(data.result){

							$scope.newchallengeform = {
						    	'title' 		:'',
						    	'description'	:'',
						    	'category'		:'',
						    	'exp_date'		:'',
						    	'tags'			:'',
						    	'rivals_comment':'',
						    	'videoUrl'		:'',
						    }
						    $scope.displayName = "Display Name";
						    
						    $scope.NameModel = "Display Name";

						    $scope.profileImage = "images/userblank.jpg";

						    document.getElementById('divLoadButton').style.display = "block";
	    					document.getElementById('divLoadVideo').style.display = "none";
	    					$scope.progressVisible = false;

	    					$scope.progressVisibleProfile = false;
	    					$scope.progressProfile = 0;

	    					alert("Video Successfully Uploaded");
	    					console.log(data);

						}else{
							alert("Opps! something went wrong.");
						}
					}
					catch(err){
						alert(err);
					}
				}).error(function(data, status, headers, config) {
					$ionicLoading.hide();
					console.log(data);
				});

	    	}
	    }

	});