angular
	.module('nav.controllers', [])
	.controller('navController', function($scope, $rootScope,$localstorage,$state){

		var isLogin = $localstorage.get("isLogin");
		if(isLogin!="1"){
			$state.go('login');
		}

		$scope.doLogout = function(){
				$localstorage.set('isLogin',0);
      			$state.go('login');
      			
				
			}

		$rootScope.$on('$stateChangeStart',function(){
	      $rootScope.stateIsLoadingNav = true;
		});

	  	$rootScope.$on('$stateChangeSuccess',function(){
	  		$rootScope.stateIsLoadingNav = false;
	  	});

	})

	

	.controller('PopoverDemoCtrl', function ($scope) {
  
	})
	

	