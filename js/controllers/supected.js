angular
	.module('suspected.controllers', [])
	.controller('suspectedController', function ($scope, $http, $localstorage, $state) {

		$scope.loadsuspected = [];
		$scope.suspecteddetail=[];

		$scope.mainURL = "";

		$scope.trustSrc = function(src) {
	        return $sce.trustAsResourceUrl(src);
	    }

  		$scope.loadsuspected1 = function(){
  			var dataset = {};
  			$http({
				method: 'POST', 
				url: '/api/allsuspected',
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				transformRequest: function(obj) {
					var str = [];
					for(var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
					return str.join("&");
				},
				data: dataset
			}).success(function(data) {
				if(data.result){
					$scope.loadsuspected = data.data;
				}else{
					alert("No user Found.");
				}
			}).error(function(data, status, headers, config) {
				
				console.log(data);
			});
  		}
  		$scope.loadsuspected1();

  		$scope.editSuspected = function(susId){

  			$localstorage.set("susId",susId);
		    $state.go('nav.editsuspected');	
		
		}

		$scope.deleteSuspected = function(id){
			var r = confirm("Are you Sure You want to delete Suspect?");
			if(r)
				{

	  			var dataset = {'user_id':id};
	  			$http({
					method: 'POST', 
					url: '/api/deleteSuspected',
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
					transformRequest: function(obj) {
						var str = [];
						for(var p in obj)
						str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
						return str.join("&");
					},
					data: dataset
				}).success(function(data) {
					if(data.result){
						alert('Suspect Deleted Successfully');
						$scope.loadsuspected1();

					}else{
						alert("Something went wrong.");
					}
				}).error(function(data, status, headers, config) {
					
					console.log(data);
				});
			}
  		}
  		

  		

	})