angular
	.module('editsuspected.controllers', [])
	.controller('editsuspectedController', function ($scope, $http, $localstorage, $state) {

		$scope.suspectId=$localstorage.get('susId');	

		$scope.editData = {};
		$scope.editData.title = "";
		$scope.editData.description = "";

		$scope.loadsuspected = function(){
  			var dataset = {'user_id':$scope.suspectId};
  			$http({
				method: 'POST', 
				url: '/api/getsuspected',
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				transformRequest: function(obj) {
					var str = [];
					for(var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
					return str.join("&");
				},
				data: dataset
			}).success(function(data) {
				if(data.result){
					$scope.editData.title = data.data[0].name;
					$scope.editData.description = data.data[0].description;
				}else{
					alert("No Suspected Found.");
				}
			}).error(function(data, status, headers, config) {
				
				console.log(data);
			});
  		}
  		$scope.loadsuspected();

  		$scope.editSuspected = function(){
  			var dataset = {'user_id': $scope.suspectId,'title':$scope.editData.title,'description':$scope.editData.description};
  			$http({
				method: 'POST', 
				url: '/api/editsuspected',
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				transformRequest: function(obj) {
					var str = [];
					for(var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
					return str.join("&");
				},
				data: dataset
			}).success(function(data) {
				
				if(data.result){
						
						$state.go('nav.suspected');

				}else{
					alert("No user Found.");
				}
			}).error(function(data, status, headers, config) {
				
				console.log(data);
			});
		
	}
  		

  		

	})