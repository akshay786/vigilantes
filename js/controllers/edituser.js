angular
	.module('edituser.controllers', [])
	.controller('edituserController', function ($scope, $http, $localstorage, $state) {

		$scope.userID=$localstorage.get('userID');	

		$scope.editData = {};
		$scope.editData.fullname = "";
		$scope.editData.number = "";

		$scope.loaduser = function(){
  			var dataset = {'user_id':$scope.userID};
  			$http({
				method: 'POST', 
				url: '/api/getuser',
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				transformRequest: function(obj) {
					var str = [];
					for(var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
					return str.join("&");
				},
				data: dataset
			}).success(function(data) {
				if(data.result){
					$scope.editData.fullname = data.data[0].fullname;
					$scope.editData.number = data.data[0].number;
				}else{
					alert("No Suspected Found.");
				}
			}).error(function(data, status, headers, config) {
				
				console.log(data);
			});
  		}
  		$scope.loaduser();

  		$scope.edituser = function(){
  			var dataset = {'user_id': $scope.userID,'fullname':$scope.editData.fullname,'number':$scope.editData.number};
  			$http({
				method: 'POST', 
				url: '/api/edituser',
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				transformRequest: function(obj) {
					var str = [];
					for(var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
					return str.join("&");
				},
				data: dataset
			}).success(function(data) {
				
				if(data.result){
						
						$state.go('nav.user');

				}else{
					alert("No user Found.");
				}
			}).error(function(data, status, headers, config) {
				
				console.log(data);
			});
		
	}
  		

  		

	})