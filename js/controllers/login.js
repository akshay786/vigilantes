angular
	.module('LoginCtrl.controllers', [])
	.controller('LoginCtrl', function ($scope, $http, $localstorage, $state) {

		var isLogin = $localstorage.get("isLogin");
		if(isLogin=="1"){
			$state.go('nav.home');
		}

		$scope.hideContent = "hide";
		$scope.hideContentMain = "";

		$scope.formData = {};
		$scope.formData.email = "";
		$scope.formData.password = "";

		$scope.doLogin = function(){
			//alert('hii');
			if($scope.formData.email==""){
			$scope.hideContent = "";
			$scope.hideContentMain = "Email is not blank";
			return false;
			}
			if($scope.formData.password==""){
				$scope.hideContent = "";
				$scope.hideContentMain = "Password is not blank";
				return false;
			}
			$scope.hideContent = "hide";
			$scope.hideContentMain = "";

			var dataset = {'email': $scope.formData.email,'pwd': $scope.formData.password};
  			$http({
				method: 'POST', 
				url: '/api/adminlogin',
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				transformRequest: function(obj) {
					var str = [];
					for(var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
					return str.join("&");
				},
				data: dataset
			}).success(function(data) {
				console.log(data);
				if(data.result){
					//alert('hello');
		    		$localstorage.set("isLogin","1");
		    		$state.go('nav.home');
		    	}else{
		    		$scope.hideContent = "";
					$scope.hideContentMain = "Invalide Credential";
		    	}
			}).error(function(data, status, headers, config) {
				console.log(data);
			});
		}


  		

	})