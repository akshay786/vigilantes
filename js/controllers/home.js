angular
	.module('HomeCtrl.controllers', [])
	.controller('HomeCtrl', function ($scope, $http, $localstorage, $state) {

		$scope.loadUser = [];

		$scope.mainURL = "";

		$scope.trustSrc = function(src) {
	        return $sce.trustAsResourceUrl(src);
	    }
})