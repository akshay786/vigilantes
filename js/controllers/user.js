angular
	.module('useCtrl.controllers', [])
	.controller('useCtrl', function ($scope, $http, $localstorage, $state) {

		$scope.loadUser = [];

		$scope.mainURL = "";

		$scope.trustSrc = function(src) {
	        return $sce.trustAsResourceUrl(src);
	    }

  		$scope.loadUser1 = function(){
  			var dataset = {};
  			$http({
				method: 'POST', 
				url: '/api/alluser',
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				transformRequest: function(obj) {
					var str = [];
					for(var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
					return str.join("&");
				},
				data: dataset
			}).success(function(data) {
				if(data.result){
					$scope.loadUser = data.data;
				}else{
					alert("No user Found.");
				}
			}).error(function(data, status, headers, config) {
				
				console.log(data);
			});
  		}
  		$scope.loadUser1();

  		$scope.edituser = function(userID){

  			$localstorage.set("userID",userID);
		    $state.go('nav.edituser');	
		
		}

		$scope.deleteuser = function(id){
			var r = confirm("Are you Sure You want to delete user?");
			if(r)
				{

	  			var dataset = {'user_id':id};
	  			$http({
					method: 'POST', 
					url: '/api/deleteUser',
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
					transformRequest: function(obj) {
						var str = [];
						for(var p in obj)
						str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
						return str.join("&");
					},
					data: dataset
				}).success(function(data) {
					if(data.result){
						alert('User Deleted Successfully');
						$scope.loadUser1();

					}else{
						alert("Something went wrong.");
					}
				}).error(function(data, status, headers, config) {
					
					console.log(data);
				});
			}
  		}

  		

	})