var express  = require('express'),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser'),
    http = require('http'),
    sha1 = require('sha1'),
    fs = require('fs'),
    cloudinary = require('cloudinary');

    AdminSchema = new mongoose.Schema({
      email     : { type: String, required: true, unique: true },
      pwd       : { type: String, required: true },
      name       : { type: String, required: true }
    }),

    Admin = mongoose.model('Admin', AdminSchema);

    UserSchema = new mongoose.Schema({
      fullname       : String,
      email          : { type: String, required: true, unique: true },
      pwd            : { type: String, required: true },
      username       : { type: String, required: true, unique: true},
      number         : { type: Number, required: true, unique: true},
      gender         : String,
      avator         : String,
      userstatus     : Boolean
    }),
    User = mongoose.model('User', UserSchema);

    Suspected = new mongoose.Schema({
      user_id               : {type: mongoose.Schema.Types.ObjectId, ref: 'User' },
      name                  : { type: String, required: true},
      description           : { type: String},
      lat                   : { type: String},
      lng                   : { type: String},
      copy_friend           : { type: String},
      image                 : { type: String},
      address               : { type: String},
      lat                   : { type: String},
      lng                   : { type: String},
      like_count            : { type: String},
      dislike_count         : { type: String},
      date_time             : { type: String}
    }),
    Suspected = mongoose.model('Suspected', Suspected);

    Like = new mongoose.Schema({
      sus_id                : {type: mongoose.Schema.Types.ObjectId, ref: 'Suspected' },
      user_id               : {type: mongoose.Schema.Types.ObjectId, ref: 'User' },
      date_time             : {type: String}
    }),
    Like = mongoose.model('Like', Like);

    Dislike = new mongoose.Schema({
      sus_id                : {type: mongoose.Schema.Types.ObjectId, ref: 'Suspected' },
      user_id               : {type: mongoose.Schema.Types.ObjectId, ref: 'User' },
      date_time             : {type: String}
    }),
    Dislike = mongoose.model('Dislike', Dislike);

    ImageSchema = new mongoose.Schema({
      path       : { type: String, required: true }
    }),

    Images = mongoose.model('Images', ImageSchema);


    Setting = new mongoose.Schema({
      user_id                    : {type: mongoose.Schema.Types.ObjectId, ref: 'User' },
      notify_all_created_actions : { type: Boolean},
      notify_only_my_actions     : { type: Boolean},
      notify_confirmed_actions   : { type: Boolean},
      distance                   : { type: String},
      enable_anonymous_posting   : { type: Boolean},
      enable_invisible_mode      : { type: Boolean}
    }),
    Setting = mongoose.model('Setting', Setting);

var multer     =       require('multer');

/*var uristring =
process.env.MONGOLAB_URI ||
process.env.MONGOHQ_URL ||
'mongodb://localhost:27017/vigilantes';*/

var uristring =
process.env.MONGOLAB_URI ||
process.env.MONGOHQ_URL ||
'mongodb://heroku_00vnmhs0:emaster@123#@ds011268.mongolab.com:11268/heroku_00vnmhs0';

var upload      =     multer({ dest: 'uploadimage/'});

var db;

mongoose.connect(uristring, function (err, res) {
  if (err) {
    console.log ('ERROR connecting to: ' + uristring + '. ' + err);
  } else {
    console.log ('Succeeded connected to: ' + uristring);
    db = res;
  }
});
/*hello*/
cloudinary.config({ 
  cloud_name: 'hxh7865ld', 
  api_key: '177311831149685', 
  api_secret: 'l8ylD8jcNSdqD1SAcYPrvl4wPVk' 
});
 
express()
  // Add headers
  .use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
})
  .use(bodyParser.json()) // support json encoded bodies
  .use(bodyParser.urlencoded({ extended: true })) // support encoded bodies
  .use(multer({ dest: 'uploadimage/',
    rename: function (fieldname, filename) {
      return filename+Date.now();
    },
    onFileUploadStart: function (file) {
      //console.log(file.originalname + ' is starting ...');
    },
    onFileUploadComplete: function (file) {
      //console.log(file.fieldname + ' uploaded to  ' + file.path)
    }
  }))
  .post('/api/adminlogin',function(req, res){
    var email       = req.body.email;
    var pwd         = req.body.pwd;

    Admin.find({'email':email, 'pwd':pwd},{},function(e,docs){
      if(docs.length!=0){
          res.send({ result: true, data: docs });
        }else{
          res.send({ result: false, data: "Invalid Credentials"});  
        }
    });
  })

  .post('/api/userlogin',function(req, res){
    var email       = req.body.email;
    var pwd         = req.body.pwd;

    User.find({'email':email, 'pwd':pwd},{},function(e,docs){
      if(docs.length!=0){
          res.send({ result: true, data: docs });
        }else{
          res.send({ result: false, data: "Invalid Credentials"});  
        }
    });
  })

  .post('/api/addadmin',function(req, res){

    var fullname    = req.body.fullname;
    var email       = req.body.email;
    var pwd         = req.body.pwd;

    var admin = new Admin({'name':fullname, 'email':email, 'pwd':pwd});
    admin.id = admin._id;
    admin.save(function (err) {
        res.send(
            (err === null) ? { result: true, data: admin  } : {result: false, msg: err }
        );
    });

  })
        

  .post('/api/adduser',function(req, res){

    var fullname           = req.body.fullname;
    var email              = req.body.email;
    var pwd                = req.body.pwd;
    var username           = req.body.username;
    var number             = req.body.number;
    var gender             = req.body.gender;
    var avator             = req.body.avator;
    var userstatus         = req.body.userstatus;

    User.find({'email':email},{},function(e,docs){
      if(docs.length!=0){
            res.send({ result: false, data:"Email already exist." });
        }else
        {
           User.find({'username':username},{},function(e,docs){
            if(docs.length!=0){
                  res.send({ result: false, data:"Username already exist." });
              }else{
                    User.find({'number':number},{},function(e,docs){
                    if(docs.length!=0){
                          res.send({ result: false, data:"Number already exist." });
                      }else
                      {
                          var user = new User({'fullname':fullname, 'email':email, 'pwd':pwd,'number':number,'gender':gender,'avator':avator,'userstatus':userstatus});
                          user.id = user._id;
                          user.save(function (err) {
                              var setting = new Setting({'user_id':user._id,"notify_all_created_actions":true, "notify_only_my_actions":true,"notify_confirmed_actions":true,"distance":'50',"enable_anonymous_posting":true,"enable_invisible_mode":true});
                              setting.id = setting._id;
                              setting.save(function (err) {
                                  /*res.send(
                                      (err === null) ? { result: true, data: setting  } : {result: false, msg: err }
                                  );*/
                              });
                              res.send(
                                  (err === null) ? { result: true, data: user  } : {result: false, msg: err }
                              );
                          });


                         
                      }
                  });
              }
          });
        }
    });


    

  })

.post('/api/addlike',function(req, res){

    var user_id                   = req.body.user_id;
    var sus_id                    = req.body.sus_id;


    var date = new Date();

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    var date_time = year + ":" + month + ":" + day + " " + hour + ":" + min + ":" + sec;

    //var newimage  = image;//"http://koodoit1.herokuapp.com/"+user_pic;

    var count=0;
    var dislike_count=0;
    Suspected.find({'_id':sus_id},{},function(e,docs){
          if(docs.like_count==null ){
             count=0;
           }else{
                count=docs.like_count;
                count++;
                dislike_count=docs.dislike_count;
                if(dislike_count==0){
                   dislike_count=0;
                }else{
                  dislike_count=docs.dislike_count;
                  dislike_count--;
                }
            }

            Suspected.update({'_id':sus_id}, {$set: {"like_count":count, "dislike_count":dislike_count}},function(e, docs){
              
          });
        
    });

    Dislike.remove({'sus_id':sus_id,'user_id':user_id},function(e,docs){
          
      });

    var like = new Like({'sus_id':sus_id,'user_id':user_id,'date_time':date_time});
    like.id = like._id;
    like.save(function (err) {
        res.send(
            (err === null) ? { result: true, data: like  } : {result: false, msg: err }
        );
    });
})


.post('/api/dislike',function(req, res){

    var user_id                 = req.body.user_id;
    var sus_id                  = req.body.sus_id;


    var date = new Date();

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    var date_time = year + ":" + month + ":" + day + " " + hour + ":" + min + ":" + sec;

    //var newimage  = image;//"http://koodoit1.herokuapp.com/"+user_pic;


    var count=0;
    var dislike_count=0;
    Suspected.find({'_id':sus_id},{},function(e,docs){
          if(docs.like_count==null ){
             count=0;
           }else{
                count=docs.dislike_count;
                dislike_count++;
                count=docs.count;
                if(count==0){
                   count=0;
                }else{
                  count=docs.count;
                  count--;
                }
            }

            Suspected.update({'_id':sus_id}, {$set: {"like_count":count, "dislike_count":dislike_count}},function(e, docs){
              
          });
        
    });

     Like.remove({'sus_id':sus_id,'user_id':user_id},function(e,docs){
          
      });

    var dislike = new Dislike({'sus_id':sus_id, 'user_id':user_id,'date_time':date_time});
    dislike.id = dislike._id;
    dislike.save(function (err) {
        res.send(
            (err === null) ? { result: true, data: dislike  } : {result: false, msg: err }
        );
    });
})


  .post('/api/addsuspected',function(req, res){

    var user_id                 = req.body.user_id;
    var name                    = req.body.name;
    var address                 = req.body.address;
    var lat                     = req.body.lat;
    var lng                     = req.body.lng;
    var copy_friend             = req.body.copy_friend;
    var description             = req.body.description;
    var image                   = req.body.image;


    var date = new Date();

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    var date_time = year + ":" + month + ":" + day + " " + hour + ":" + min + ":" + sec;

    //var newimage  = image;//"http://koodoit1.herokuapp.com/"+user_pic;
    

    var suspected = new Suspected({'user_id':user_id, 'name':name, 'address':address,'lat':lat,'lng':lng,'copy_friend':copy_friend,'description':description,'image':image,'date_time':date_time,'like_count':'0','dislike_count':'0'});
    suspected.id = suspected._id;
    suspected.save(function (err) {
        res.send(
            (err === null) ? { result: true, data: suspected  } : {result: false, msg: err }
        );
    });
})

.post('/api/getlike',function(req, res){
    var user_id       = req.body.user_id;
    var sus_id        = req.body.sus_id;
    Like.find({'user_id':user_id,'sus_id':sus_id},{},function(e,docs){
      if(docs.length!=0){
          res.send({ result: true, data: docs });
         
       }else{
          res.send({ result: false, data: "Invalid Credentials"});  
        }
    });
  })

.post('/api/allgetlike',function(req, res){
    var user_id       = req.body.user_id;
    var sus_id        = req.body.sus_id;
    Like.find({},{},function(e,docs){
      if(docs.length!=0){
          res.send({ result: true, data: docs });
         
       }else{
          res.send({ result: false, data: "Invalid Credentials"});  
        }
    });
  })

.post('/api/getdislike',function(req, res){
    var user_id       = req.body.user_id;
    var sus_id        = req.body.sus_id;
    Dislike.find({'user_id':user_id,'sus_id':sus_id},{},function(e,docs){
      if(docs.length!=0){
          res.send({ result: true, data: docs });
         
       }else{
          res.send({ result: false, data: "Invalid Credentials"});  
        }
    });
  })

.post('/api/setting',function(req, res){

    var user_id                     = req.body.user_id;
    var notify_all_created_actions  = req.body.notify_all_created_actions;
    var notify_only_my_actions      = req.body.notify_only_my_actions;
    var notify_confirmed_actions    = req.body.notify_confirmed_actions;
    var distance                    = req.body.distance;
    var enable_anonymous_posting    = req.body.enable_anonymous_posting;
    var enable_invisible_mode       = req.body.enable_invisible_mode;

    Setting.find({'user_id':user_id},{},function(e,docs){
      if(docs.length!=0){

            Setting.update({'user_id':user_id}, {$set: {"notify_all_created_actions":notify_all_created_actions, "notify_only_my_actions":notify_only_my_actions,"notify_confirmed_actions":notify_confirmed_actions,"distance":distance,"enable_anonymous_posting":enable_anonymous_posting,"enable_invisible_mode":enable_invisible_mode}},function(e, docs){
              res.send(
                (e === null) ? { result: true, data: docs  } : {result: false, msg: e }
              );
          });
            
        }else
        {
          var setting = new Setting({'user_id':user_id,"notify_all_created_actions":notify_all_created_actions, "notify_only_my_actions":notify_only_my_actions,"notify_confirmed_actions":notify_confirmed_actions,"distance":distance,"enable_anonymous_posting":enable_anonymous_posting,"enable_invisible_mode":enable_invisible_mode});
            setting.id = setting._id;
            setting.save(function (err) {
                res.send(
                    (err === null) ? { result: true, data: setting  } : {result: false, msg: err }
                );
            });
        }
    });

})

.post('/api/getsetting',function(req, res){
    var user_id       = req.body.user_id;
    Setting.find({'user_id':user_id},{},function(e,docs){
      if(docs.length!=0){
          res.send({ result: true, data: docs });
         
       }else{
          res.send({ result: false, data: "Invalid Credentials"});  
        }
    });
  })

.post('/api/allsetting',function(req, res){
    Setting.find({},{},function(e,docs){
      if(docs.length!=0){
          res.send({ result: true, data: docs });
         
       }else{
          res.send({ result: false, data: "Invalid Credentials"});  
        }
    });
  })

.post('/api/addimage',function(req, res){

    var image    = req.body.image;

    cloudinary.uploader.upload(image, function(result) { 
    var path = result.secure_url;

    var images = new Images({'path':image});
    images.id = images._id;
    images.save(function (err) {
        res.send(
            (err === null) ? { result: true, data: images  } : {result: false, msg: err }
        );
    });

  });

})

    .post('/api/allimage',function(req, res){
      Images.find({},{},function(e,docs){
          res.send(
            (e === null) ? { result: true, data: docs  } : {result: false, msg: e }
          );
      });
  })

  .post('/api/alluser',function(req, res){
      User.find({},{},function(e,docs){
          res.send(
            (e === null) ? { result: true, data: docs  } : {result: false, msg: e }
          );
      });
  })

  .post('/api/allsuspected',function(req, res){
      Suspected.find({},{},function(e,docs){
          res.send(
            (e === null) ? { result: true, data: docs  } : {result: false, msg: e }
          );
      });
  })

  .post('/api/getsuspected',function(req, res){
    var sus_id       = req.body.sus_id;
    Suspected.find({'_id':sus_id},{},function(e,docs){
      if(e==null){
          res.send({ result: true, data: docs });
         
       }else{
          res.send({ result: false, data: "Invalid Credentials"});  
        }
    });
  })

  .post('/api/editsuspected',function(req, res){
    var user_id       = req.body.user_id;
    var title       = req.body.title;
    var description       = req.body.description;

    Suspected.update({'_id':user_id}, {$set: {"name":title, "description":description}},function(e, docs){
            res.send(
              (e === null) ? { result: true, data: docs  } : {result: false, msg: e }
            );
        });
  })

  .post('/api/getuser',function(req, res){
    var user_id       = req.body.user_id;

    User.find({'_id':user_id},{},function(e,docs){
      if(docs.length!=0){
          res.send({ result: true, data: docs });
        }else{
          res.send({ result: false, data: "Invalid Credentials"});  
        }
    });
  })

  .post('/api/getimage',function(req, res){
    var image_id       = req.body.image_id;

    Images.find({'_id':image_id},{},function(e,docs){
      if(e==null){
          res.send({ result: true, data: docs });
        }else{
          res.send({ result: false, data: "Invalid Credentials"});  
        }
    });
  })

  .post('/api/edituser',function(req, res){
    var user_id       = req.body.user_id;
    var fullname       = req.body.fullname;
    var username       = req.body.username;
    var number       = req.body.number;

    User.update({'_id':user_id}, {$set: {"fullname":fullname, "username":username, "number":number}},function(e, docs){
            res.send(
              (e === null) ? { result: true, data: docs  } : {result: false, msg: e }
            );
        });
  })

  .post('/api/deleteUser',function(req, res){
      var user_id       = req.body.user_id;
      User.remove({'_id':user_id},function(e,docs){
          res.send(
            (e === null) ? { result: true, data: docs  } : {result: false, msg: e }
          );
      });
  })

  .post('/api/deleteImage',function(req, res){
      Images.remove({},function(e,docs){
          res.send(
            (e === null) ? { result: true, data: docs  } : {result: false, msg: e }
          );
      });
  })

  .post('/api/deleteSuspected',function(req, res){
      var user_id       = req.body.user_id;
      Suspected.remove({'_id':user_id},function(e,docs){
          res.send(
            (e === null) ? { result: true, data: docs  } : {result: false, msg: e }
          );
      });
  })

  .post('/api/AlldeleteSuspected',function(req, res){
      var user_id       = req.body.user_id;
      Suspected.remove({},function(e,docs){
          res.send(
            (e === null) ? { result: true, data: docs  } : {result: false, msg: e }
          );
      });
  })
  

  .post('/api/forgotpassword',function(req, res){

      var email       = req.body.email;

      User.find({'email':email},{},function(e,docs){
      if(docs.length!=0){

            var pwd = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            for( var i=0; i < 5; i++ )
            {
                pwd += possible.charAt(Math.floor(Math.random() * possible.length));
            }

             var options = {
              host: 'engineermaster.in',
              port: null,
              path: '/vigilantes/mail.php?email='+email+'&pwd='+pwd
            };
            http.get(options, function(resp){
              resp.on('data', function(chunk){
                    User.update({'email':email}, {$set: {"pwd":pwd}},function(e, docs){
                        res.send(
                          (e === null) ? { result: true, data: docs  } : {result: false, msg: e }
                        );
                    });
              });
            }).on("error", function(e){
              console.log("Got error: " + e.message);
            });
        }else{
          res.send({ result: false, data: "Invalid Email"});  
        }
    });
})

.use(express.static(__dirname + '/'))
.listen(process.env.PORT || 5000);
